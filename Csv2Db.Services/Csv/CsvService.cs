﻿using Csv2Db.Services.Helpers;
using Csv2Db.Services.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Csv2Db.Services.Csv
{
    public class CsvService: ICsvService
    {
        public T LoadCsv<T>(Stream stream)
        {
            var model = Activator.CreateInstance<T>();

            var propertiesWithCsvMeta = CsvHelper.GetPropertiesWithCsvMeta(model);

            using (var reader = new StreamReader(stream))
            {
                var columns = ParseLine(reader);
                var columnMappings = GetColumnMappings(columns, propertiesWithCsvMeta);

                string[] row = null;
                while ((row = ParseLine(reader)) != null)
                {
                    ProcessCsvRow<T>(columnMappings, model, propertiesWithCsvMeta, row);
                }
            }

            return model;
        }

        private string[] ParseLine(StreamReader reader)
        {
            var line = reader.ReadLine();
            if (string.IsNullOrEmpty(line))
            {
                return null;
            }

            var items = line.Split(',');

            return items;
        }

        private List<CsvColumnMapping> GetColumnMappings(string[] columns, List<PropertyWithCsvMeta> propertiesWithCsvMeta)
        {
            var columnMappings = new List<CsvColumnMapping>();
            foreach (var column in columns)
            {
                var columnDef = column.Split('.');
                var entityName = columnDef[0];
                var propertyName = columnDef[1];

                var propertyWithCsvMeta = propertiesWithCsvMeta.FirstOrDefault(x => string.Equals(x.EntityType.Name, entityName));

                columnMappings.Add(new CsvColumnMapping
                {
                    PropertyName = propertyName,
                    EntityName = entityName,
                });
            }

            return columnMappings;
        }

        private void ProcessCsvRow<T>(List<CsvColumnMapping> columnMappings, T model, List<PropertyWithCsvMeta> propertiesWithCsvMeta, string[] row)
        {
            propertiesWithCsvMeta.ForEach(x => { x.Entity = null; });

            var i = 0;
            foreach (var columnMapping in columnMappings)
            {
                var p = propertiesWithCsvMeta.FirstOrDefault(x => x.EntityType.Name == columnMapping.EntityName);
                if (p.Entity == null)
                {
                    p.Entity = Activator.CreateInstance(p.EntityType);
                }

                var property = p.EntityType.GetProperty(columnMapping.PropertyName);
                var value = Convert.ChangeType(row[i], property.PropertyType);
                property.SetValue(p.Entity, value);
                i++;
            }

            foreach (var propertyWithCsvMeta in propertiesWithCsvMeta)
            {
                var list = propertyWithCsvMeta.PropertyInfo.GetValue(model) as IList;
                if (list == null)
                {
                    list = (IList)Activator.CreateInstance(propertyWithCsvMeta.PropertyInfo.PropertyType);
                    propertyWithCsvMeta.PropertyInfo.SetValue(model, list);
                }
                list.Add(propertyWithCsvMeta.Entity);
            }
        }
    }
}
