﻿using System.IO;

namespace Csv2Db.Services.Csv
{
    public interface ICsvService
    {
        T LoadCsv<T>(Stream stream);
    }
}
