﻿using Csv2Db.Entities;
using Csv2Db.Services.Helpers;
using Microsoft.EntityFrameworkCore;
using System;

namespace Csv2Db.Services.Db
{
    public class DbService : IDbService, IDisposable
    {
        private readonly DbEntities _dbEntities;

        public DbService(DbEntities dbEntities)
        {
            _dbEntities = dbEntities;
        }

        public void Dispose()
        {
            _dbEntities.Dispose();
        }

        public void Clear()
        {
            _dbEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [LoanRepayments]");
            _dbEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [UserLoans]");
            _dbEntities.Database.ExecuteSqlCommand("TRUNCATE TABLE [Users]");
        }

        public void Save<T>(T model)
        {
            var propertiesWithCsvMeta = CsvHelper.GetPropertiesWithCsvMeta(model);

            foreach (var propertyWithCsvMeta in propertiesWithCsvMeta)
            {
                var entitySetName = propertyWithCsvMeta.PropertyInfo.Name;
                var entitySetValue = propertyWithCsvMeta.PropertyInfo.GetValue(model) as dynamic;

                var entitySet = _dbEntities.GetType().GetProperty(entitySetName).GetValue(_dbEntities) as dynamic;
                entitySet.AddRange(entitySetValue);
            }
            _dbEntities.SaveChanges();
        }
    }
}
