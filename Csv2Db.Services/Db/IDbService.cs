﻿namespace Csv2Db.Services.Db
{
    public interface IDbService
    {
        void Clear();
        void Save<T>(T model);
    }
}
