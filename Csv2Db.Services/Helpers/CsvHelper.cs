﻿using Csv2Db.Services.Attributes;
using Csv2Db.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Csv2Db.Services.Helpers
{
    public class CsvHelper
    {
        public static List<PropertyWithCsvMeta> GetPropertiesWithCsvMeta(object model)
        {
            var propertiesWithCsvMeta = model
                .GetType()
                .GetProperties()
                .Where(x => x.GetCustomAttribute<CsvMetaAttribute>() != null)
                .Select(x => new PropertyWithCsvMeta
                {
                    PropertyInfo = x,
                    EntityType = x.PropertyType.GetGenericArguments().FirstOrDefault()
                })
                .ToList();

            return propertiesWithCsvMeta;
        }
    }
}
