﻿using Csv2Db.Entities;
using Csv2Db.Services.Attributes;
using System.Collections.Generic;

namespace Csv2Db.Services.Models
{
    public class LoanRepaymentsCsvModel
    {
        [CsvMeta]
        public List<LoanRepayment> LoanRepayments { get; set; }
    }
}
