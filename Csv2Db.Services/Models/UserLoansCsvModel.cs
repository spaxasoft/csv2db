﻿using Csv2Db.Entities;
using Csv2Db.Services.Attributes;
using System.Collections.Generic;

namespace Csv2Db.Services.Models
{
    public class UserLoansCsvModel
    {
        [CsvMeta]
        public List<User> Users { get; set; }

        [CsvMeta]
        public List<UserLoan> UserLoans { get; set; }
    }
}
