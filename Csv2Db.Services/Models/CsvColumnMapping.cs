﻿namespace Csv2Db.Services.Models
{
    public class CsvColumnMapping
    {
        public string PropertyName { get; set; }
        public string EntityName { get; set; }
    }
}
