﻿using System;
using System.Reflection;

namespace Csv2Db.Services.Models
{
    public class PropertyWithCsvMeta
    {
        public PropertyInfo PropertyInfo { get; set; }
        public Type EntityType { get; set; }
        public object Entity { get; set; }
    }
}
