The following assumptions have been made based on keeping the sample
very simple, readable, unit testable

Postman collection is located at /docs/postman
Db backup (LocalDb (SqlExpress)) is located at /docs/db
Connection string needs to change if restored on any sql versions other than LocalDb
2 Csv files (for test in postman) are located at /docs/files

I did not include unit tests because in the test requirements wasn't mentioned.
Obviously that's a necessory chunk of the solution.

In order to extend this to cover new csv file uploads:
E.g. we need to have a new csv for uploading Lenders data
1- Create Lenders table in db e.g with columns LenderId, Name
2- Csv2Db.Entities-> Create entity class Lender.cs
3- Csv2Db.Services.Models-> create a model for Lenders upload
    public class LendersCsvModel
    {
        [CsvMeta]
        public List<Lender> Lenders { get; set; }
    }
4- Csv2Db.Apis.Enums->add new enum member to CsvFileTypes
	e.g. Lenders
5- Csv2Db.Apis.Controllers.CsvController->FileUpload-> add new case
	case CsvFileTypes.Lenders:
        HandleCsvLenders(file);
		break;
		
6- In the same file append the following method         
	private void HandleCsvLenders(IFormFile file)
	{
		using (var fs = file.OpenReadStream())
		{
			var model = _csvService.LoadCsv<LendersCsvModel>(fs);
			_dbService.Save(model);
		}
	}

I tried to keep it very simple to be easily readable for the sake of technical test
That's why I did not include
1- Logging e.g. from Microsoft.Extensions.Logging
2- AutoMapper
3- Global error handling
4- Authentication/Authorization using BasicAuth or OAuth2 or ...
5- I also commented Startup->UseHttpsRedirection for the sake of easier testing using postman

In terms of functionality I am aware that a fail safe operation should be there so that it could try to find the record in the db first and if not found then create a new record 
But again for the sake of keeping it simple I ignored this.
So if retesting please clear db first
There is an endpoint that i created for that. DELETE /api/db/
This is just for test. In real world we never play/expose db operations using endpoints directly, specially very scary operations such as truncate.

I did not create any UI-Angular project because i wasn't sure it was required
Please let me know if I need to add that too.

Hitting Apis using Postman
	1. POST api/csv/userloans
		Just attach the csv file (from /docs/files) to the body as form-data
		Adds records to both Users and UserLoans tables
	2. POST api/csv/loanrepayments
		Just attach the csv file (from /docs/files) to the body as form-data
		Adds records to loanrepayments table
	3. DELETE api/db
		truncates tables in the db (just for the sake of this technical test)
	4. GET api/heartbeat
		to be used in load balancers or server's scheduled tasks or manually to make sure the apis are up and running
		
Please note that using postman for the first time to upload csv files, you need to actually select the csv file again before sending the request.
This is a postman bug. It does not save the file with the request but shows the path and user thinks that it is attached to the request body but it's not. you need to actually select the file again	
	