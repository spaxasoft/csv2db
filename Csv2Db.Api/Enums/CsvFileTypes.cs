﻿namespace Csv2Db.Api.Enums
{
    public enum CsvFileTypes
    {
        UserLoans,
        LoanRepayments
    }
}
