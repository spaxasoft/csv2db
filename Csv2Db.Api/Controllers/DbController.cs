﻿using Csv2Db.Services.Csv;
using Csv2Db.Services.Db;
using Microsoft.AspNetCore.Mvc;

namespace Csv2Db.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DbController : ControllerBase
    {
        private readonly ICsvService _csvService;
        private readonly IDbService _dbService;

        public DbController(ICsvService csvService, IDbService dbService)
        {
            _csvService = csvService;
            _dbService = dbService;
        }

        [HttpDelete]
        [Route("")]
        public IActionResult ClearDatabase()
        {
            _dbService.Clear();

            return Ok();
        }
    }
}
