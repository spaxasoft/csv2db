﻿using Csv2Db.Api.Enums;
using Csv2Db.Services.Csv;
using Csv2Db.Services.Db;
using Csv2Db.Services.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Csv2Db.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CsvController : ControllerBase
    {
        private readonly ICsvService _csvService;
        private readonly IDbService _dbService;

        public CsvController(ICsvService csvService, IDbService dbService)
        {
            _csvService = csvService;
            _dbService = dbService;
        }

        [HttpPost()]
        [Route("{csvFileType}")]
        public IActionResult FileUpload([FromRoute]CsvFileTypes csvFileType, IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return BadRequest();                
            }

            using (var fs = file.OpenReadStream())
            {
                switch (csvFileType)
                {
                    case CsvFileTypes.UserLoans:
                        HandleCsvUserLoans(file);
                        break;

                    case CsvFileTypes.LoanRepayments:
                        HandleCsvLoanRepayments(file);
                        break;

                }
            }
            return Ok();
        }

        private void HandleCsvUserLoans(IFormFile file)
        {
            using (var fs = file.OpenReadStream())
            {
                var model = _csvService.LoadCsv<UserLoansCsvModel>(fs);
                _dbService.Save(model);
            }
        }

        private void HandleCsvLoanRepayments(IFormFile file)
        {
            using (var fs = file.OpenReadStream())
            {
                var model = _csvService.LoadCsv<LoanRepaymentsCsvModel>(fs);
                _dbService.Save(model);
            }
        }
    }
}
