﻿using Microsoft.AspNetCore.Mvc;

namespace Csv2Db.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeartbeatController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public IActionResult Heartbeat() => Ok();
    }
}
