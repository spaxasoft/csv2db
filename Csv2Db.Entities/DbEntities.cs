﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Csv2Db.Entities
{
    public class DbEntities : DbContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionStringName;

        public DbEntities(IConfiguration configuration, string connectionStringName = "DefaultConnection")
        {
            _configuration = configuration;
            _connectionStringName = connectionStringName;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserLoan> UserLoans { get; set; }
        public DbSet<LoanRepayment> LoanRepayments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString(_connectionStringName));
        }
    }
}
