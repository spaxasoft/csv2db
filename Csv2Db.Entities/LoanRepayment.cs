﻿using System;

namespace Csv2Db.Entities
{
    public class LoanRepayment
    {
        public int LoanRepaymentId { get; set; }
        public int UserLoanId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
    }
}
