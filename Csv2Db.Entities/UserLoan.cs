﻿using System;

namespace Csv2Db.Entities
{
    public class UserLoan
    {
        public int UserLoanId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public decimal Amount { get; set; }
        public decimal InterestRate { get; set; }
    }
}
